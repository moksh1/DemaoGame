﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstaclesController : MonoBehaviour
{
    public ObstaclesView obstaclesViewObj;
    float nextSpawnRats = 0.0f;
    float nextSpawnDogs = 0.0f;
    void Start()
    {
        for (int i = 0; i < 3; i++)
        {
            float RandomX = Random.Range(-6.3f, 6.3f);
            float RandomY = Random.Range(-4.19f, 4.19f);
            obstaclesViewObj.InitSpawn(RandomX,RandomY);
        }
    }

    void Update()
    {
        if(Time.time > nextSpawnRats)
        {
            float RandomX = Random.Range(-6.3f, 6.3f);
            float RandomY = Random.Range(-4.19f, 4.19f);
            nextSpawnRats = Time.time + 2f;
            obstaclesViewObj.Spawn(ObstacleType.RAT,RandomX,RandomY);
        }
        if(Time.time > nextSpawnDogs)
        {
            nextSpawnDogs = Time.time + 10f;
            float RandomX = Random.Range(-6.3f, 6.3f);
            float RandomY = Random.Range(-4.19f, 4.19f);
            obstaclesViewObj.Spawn(ObstacleType.DOG,RandomX,RandomY);
        }
    }
}
