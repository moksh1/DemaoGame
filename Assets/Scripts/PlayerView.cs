﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerView : MonoBehaviour
{
    public PlayerController playerControllerObj;
    public Rigidbody2D player;
    void Update()
    {
        player.transform.Translate(Vector2.up * playerControllerObj.speed * Time.fixedDeltaTime);
    }
    public void SetRotation(float angle)
    {
        player.transform.localRotation = Quaternion.AngleAxis(angle, Vector3.forward);
    }

    private void HandleObstaceCollision(ObstacleModel model)
    {

        if (model.type == ObstacleType.RAT)
        {
            Destroy(model.gameObject);
            playerControllerObj.ScoreIncrementSignal();
        }
        else if (model.type == ObstacleType.DOG)
        {
            Destroy(model.gameObject);
            playerControllerObj.LifeDecrementSignal();
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        ObstacleModel obstacle = collision.gameObject.GetComponent<ObstacleModel>();
        if (obstacle != null)
        {
            HandleObstaceCollision(obstacle);
        }
        if (collision.gameObject.name == "Wall")
        {
            if (Input.GetMouseButton(0))
            {
                playerControllerObj.InvertRotation();
            }
            else
            {
                player.transform.rotation = Quaternion.AngleAxis(player.transform.eulerAngles.z + 180f, Vector3.forward);
            }
        }
    }

}
