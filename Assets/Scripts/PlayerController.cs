﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public WheelController wheelControllerObj;
    public GameController gameControllerObj;
    public PlayerView playerViewObj;
    public float speed = 5f;
    public float GetPlayerZAxis()
    {
        return playerViewObj.player.transform.eulerAngles.z;
    }
    public void UpdateRotation(float angle)
    {
        //playerViewObj.MovePlayer(Vector2.up,speed);
        playerViewObj.SetRotation(angle);
    }
    public void ScoreIncrementSignal()
    {
        gameControllerObj.ScoreIncrement();
    }
    public void InvertRotation()
    {
        wheelControllerObj.RefreshAngleValue = true;
    }
    public void LifeDecrementSignal()
    {
        gameControllerObj.LifeDecrement();
    }
}
